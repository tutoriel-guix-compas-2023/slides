(require 'org)
;; Global publishing functions
(require 'ox-publish)
;; LaTeX publishing functions
(require 'ox-latex)
;; Support for citations
(require 'oc)
(require 'oc-biblatex)

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Do not prompt for code block evaluation.
(setq org-confirm-babel-evaluate nil)

;; Force publishing of unchanged files.
(setq org-publish-use-timestamps-flag nil)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)))

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process
      (list "latexmk --shell-escape -f -pdf -%latex -interaction=nonstopmode
-output-directory=%o %f"))

;; Use BibLaTeX citation processor by default.
(setq org-cite-export-processors '((t biblatex)))

;; Configure LaTeX to use 'minted' for code block export.
(setq org-latex-packages-alist '())
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
        '(("linenos = false") ("mathescape") ("breaklines")
          ("style = monokai")))

;; Load additional LaTeX packages allowing us to:
;;   - use scalable vector graphics (without exporting them through LaTeX),
(add-to-list 'org-latex-packages-alist '("inkscapelatex = false" "svg"))
;;   - use Helvetica as the default sans-serif font,
(add-to-list 'org-latex-packages-alist '("" "helvet"))
;;   - use smileys,
(add-to-list 'org-latex-packages-alist '("" "tikzsymbols"))
;;   - use the dedicated Guix Beamer theme with Inria logo.
(add-to-list 'org-latex-packages-alist '("inria" "beamerthemeguix"))

;; Define publishing targets.
(setq org-publish-project-alist
      (list
       (list "slides"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["slides.org"]
             :publishing-function '(org-beamer-publish-to-pdf)
             :publishing-directory "./public")))

(provide 'publish)
