# Tutoriel Guix - Compas 2023: Slides

[![pipeline status](https://gitlab.inria.fr/tutoriel-guix-compas-2023/slides/badges/master/pipeline.svg)](https://gitlab.inria.fr/tutoriel-guix-compas-2023/slides/-/commits/master)

Vers une étude scientifique expérimentale reproductible

[Show slides](https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/slides/slides.pdf)

